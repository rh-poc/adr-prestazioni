ADR PoC Use case 3
==============================================

Processes
----------------------------------------------

### Inserimento prestazione/lavoro

![BPMN](src/main/resources/com/adr/prestazioni/prestazioni.carica-prestazione-svg.svg)

### Ricezione fattura

![BPMN](src/main/resources/com/adr/prestazioni/prestazioni.ricezione-fattura-svg.svg)

#### Dubbi implementazione

- "apertura EM" è la stessa attività sia che la verifica positiva che negativa, si può mettere a fattor comune? 
- se la verifica è positiva, allora dopo l'apertura EM si può saltare alla "registrazione fattura"?